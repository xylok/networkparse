init:
	pipenv install --dev

test: doctest
	poetry run pytest --cov=networkparse

build:
	python3 setup.py sdist bdist_wheel

docs:
	poetry run sphinx-build -b html docs/ docs/_build/html

doctest:
	poetry run sphinx-build -M doctest docs/ docs/_build/html

.PHONY: init test build docs doctest
