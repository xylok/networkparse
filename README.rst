Installing
==========

.. code-block::

    pip install --user networkparse

Using
=====
Docs are available on `Read the Docs`_

.. _`Read the Docs`: https://networkparse.readthedocs.io/en/latest/


License
=======
This module was developed by and for Xylok_. The code is release under the MIT license.

.. _Xylok: https://www.xylok.io


Credits
=======
This module was inspired by CiscoConfParse_.

.. _CiscoConfParse: https://github.com/mpenning/ciscoconfparse
