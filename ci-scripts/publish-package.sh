#!/bin/sh
. ./ci-scripts/aliases.sh

docker pull "$NETWORKPARSE_TEST_IMAGE" || exit 1
docker run \
      $(./ci-scripts/env-to-docker-run.sh)  \
      "$NETWORKPARSE_TEST_IMAGE" \
      sh ci-scripts/publish-helper.sh || exit 1
