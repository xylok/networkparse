#!/bin/sh
# Source this file to setup aliases
alias envsubst='./ci-scripts/envsubst.sh'

function python3() {
  docker run --rm -i \
      $(./ci-scripts/env-to-docker-run.sh)  \
      -w /working/ \
      -v "$(pwd):/working/" \
      python:3.6 \
      python3 "$@"
}
