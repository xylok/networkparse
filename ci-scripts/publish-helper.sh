#!/bin/sh
# python3 setup.py sdist bdist_wheel || exit 1
# twine upload dist/* || exit 1
poetry build || exit 1
poetry publish --username="$TWINE_USERNAME" --password="$TWINE_PASSWORD" || exit 1
