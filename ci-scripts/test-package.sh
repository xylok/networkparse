#!/bin/sh
. ./ci-scripts/aliases.sh

docker pull "$NETWORKPARSE_TEST_IMAGE" || exit 1
docker run "$NETWORKPARSE_TEST_IMAGE" poetry run black --check . || exit 1
docker run "$NETWORKPARSE_TEST_IMAGE" poetry run pytest -p no:cacheprovider --cov=networkparse || exit 1
# docker run \
#       $(./ci-scripts/env-to-docker-run.sh)  \
#       "$NETWORKPARSE_TEST_IMAGE" python3 setup.py check sdist bdist || exit 1
docker run "$NETWORKPARSE_TEST_IMAGE" poetry run sphinx-build -M doctest docs docs/_build || exit 1
docker tag "$NETWORKPARSE_TEST_IMAGE" "$NETWORKPARSE_RELEASE_IMAGE" || exit 1
docker push "$NETWORKPARSE_RELEASE_IMAGE" || exit 1
