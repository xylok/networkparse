#!/bin/sh
# Create latest/test images for PP
. ./ci-scripts/aliases.sh

docker pull "$NETWORKPARSE_LATEST_IMAGE"
docker build \
  --cache-from "$NETWORKPARSE_LATEST_IMAGE" \
  --build-arg "VERSION=$XYLOK_VERSION" \
  -t "$NETWORKPARSE_LATEST_IMAGE" \
  . || exit 1
docker push "$NETWORKPARSE_LATEST_IMAGE" || exit 1
docker tag "$NETWORKPARSE_LATEST_IMAGE" "$NETWORKPARSE_TEST_IMAGE" || exit 1
docker push "$NETWORKPARSE_TEST_IMAGE" || exit 1
