#!/bin/sh
docker run --rm -i \
    $(./ci-scripts/env-to-docker-run.sh) \
    traherom/kustomize-docker:1 \
    envsubst "$@"
