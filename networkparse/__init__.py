"""
A simple network configuration searcher
"""
from pathlib import Path

from . import core
from . import parse
