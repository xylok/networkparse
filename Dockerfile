FROM python:3.7-slim-buster
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir /code
WORKDIR /code/

RUN adduser --uid 1000 --shell /bin/bash --gecos '' --disabled-password appuser && \
  apt-get update && \
  apt-get -y install \
    git wget make \
    gcc \
    g++ \
    python3-dev \
    libffi-dev \
    libssl-dev && \
  apt-get clean && \
  pip install -U pip && \
  pip install poetry && \
  poetry config virtualenvs.create false && \
  cp -r ~/.config/ / && \
  chmod -R a+r /.config
COPY pyproject.toml poetry.lock /
RUN poetry install 

# Code
ADD . /code/

# Run by default
CMD pytest

