.. Network Parse documentation master file, created by
   sphinx-quickstart on Sat Sep  1 17:27:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Network Parse's documentation!
=========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   tutorial
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
