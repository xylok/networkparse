.. _api:

============
Complete API
============

Parsing
------------------------------------
.. automodule:: networkparse.parse

Automatic Parsing
++++++++++++++++++++++++++

.. autofunction:: networkparse.parse.automatic


Base Configuration Manager
++++++++++++++++++++++++++

:class:`~ConfigBase` will almost never be directly created, but it's functionality is
shared by all other configuration classes. To avoid duplicate documentation, refer back
to this class for complete details on what a configuration type offers.

.. autoclass:: networkparse.parse.ConfigBase
      :members:

Cisco
++++++

.. autoclass:: networkparse.parse.ConfigIOS
      :members:

.. autoclass:: networkparse.parse.ConfigNXOS
      :members:

.. autoclass:: networkparse.parse.ConfigASA
      :members:


Fortinet
++++++++

.. autoclass:: networkparse.parse.ConfigFortigate
      :members:


HP
++
.. autoclass:: networkparse.parse.ConfigHPCommware
      :members:


Juniper
+++++++

.. autoclass:: networkparse.parse.ConfigJunos
      :members:


Parsing Utils
------------------------------------
.. autoclass:: networkparse.parse.VersionNumber
      :members:
      :inherited-members:


Searching
------------------------------------
Once a :class:`~networkparse.core.ConfigBase` has been created, searching is
typically done using :class:`~networkparse.parse.ConfigLineList`
and :class:`~networkparse.parse.ConfigLine`.

.. autoclass:: networkparse.core.ConfigLineList
      :members:
      :inherited-members:

.. autoclass:: networkparse.core.ConfigLine
      :members:
      :inherited-members:

Exceptions
------------------------------------
.. autoclass:: networkparse.core.MultipleLinesError

.. autoclass:: networkparse.core.NoLinesError
