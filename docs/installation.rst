.. _installation:

===============
Getting Started
===============

`networkparse` is requires Python 3.6, but has no other dependencies.

.. code:: bash

    pip install --user networkparse
