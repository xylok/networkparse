from textwrap import dedent
import pytest

from .context import parse, core


@pytest.fixture
def search_config():
    text = dedent(
        """
        child 1
        child 2
        child 3
            child 4
            child 5
        child 6
            child 7
                child 8
                child 9
            child 7 next sibling
        child 10
        silly child 11
        sibling
        dupe
            child 12
        dupe
            child 13
        ! child 14
        !   child 15
        """
    ).strip()
    return parse.ConfigIOS(text)


def config_checker(parent):
    """
    Ensure all children point to their parents, children don't have leading spaces, etc
    """
    for c in parent.children:
        assert c.text == c.text.strip(), "Children should not have useless spaces"
        assert c.parent == parent, "Children must point to their parent"
        config_checker(c)


def test_list_filter(search_config):
    config = search_config
    assert len(config.filter("child")) == 0
    assert len(config.filter("child", full_match=False)) == 6
    assert len(config.filter("child", full_match=False, depth=None)) == 14
    assert len(config.filter("child", depth=None)) == 0
    assert len(config.filter("sibling")) == 1

    assert len(config.filter("child 1.*")) == 2
    assert len(config.filter("CHILD 1.*")) == 0, "Ignoring case incorrectly"
    assert (
        len(config.filter("CHILD 1.*", ignore_case=True)) == 2
    ), "Not ignoring case correctly"
    assert len(config.filter(".*child 1.*")) == 3
    assert len(config.filter("child 1.*", full_match=False)) == 3

    assert len(config.filter(".*14.*", skip_comments=False)) == 1


def test_list_exclude(search_config):
    config = search_config
    assert len(config.exclude("child 1.*")) == 7
    assert len(config.exclude("child 1.*", depth=None)) == 13


def test_list_flatten(search_config):
    config = search_config
    assert len(config.flatten()) == 19
    assert len(config.flatten(depth=1)) == 17


def test_filter_children_comments(search_config):
    config = search_config
    assert (
        len(config.filter("child.*", full_match=False)) == 6
    ), "Comments should not match a filter"
    # assert len(config.filter("child.*", full_match=False, skip_comments=False)) == 8


def test_siblings(search_config):
    config = search_config
    some_item = config.filter("child 1").one()
    siblings = some_item.siblings
    assert config == some_item.parent
    assert len(config) - 1 == len(siblings)
    assert isinstance(siblings, parse.ConfigLineList)
    assert some_item not in siblings


def test_get_top(search_config):
    config = search_config
    already_top_item = config.filter("child 6").one()
    assert already_top_item.get_top() is already_top_item

    deep_item = config.filter("child 9", depth=None).one()
    assert deep_item.get_top() is already_top_item


def test_is_top(search_config):
    config = search_config
    top_item = config.filter("child 6").one()
    assert top_item.is_top()

    deep_item = config.filter("child 9", depth=None).one()
    assert not deep_item.is_top()


def test_get_next(search_config):
    config = search_config
    child_7 = config.filter("child 7", depth=None).one()
    child_7_real_next = config.filter("child 8", depth=None).one()
    assert child_7.get_next() is child_7_real_next

    last_item = config.filter("child 13", depth=None).one()
    assert last_item.get_next() is None


def test_get_next_sibling(search_config):
    config = search_config
    next_line_is_next_sibling = config.filter("child 4", depth=None).one()
    next_sibling = config.filter("child 5", depth=None).one()
    assert next_line_is_next_sibling.get_next_sibling() is next_sibling

    last_sibling = config.filter("child 5", depth=None).one()
    assert last_sibling.get_next_sibling() is None

    no_siblings = config.filter("child 12", depth=None).one()
    assert no_siblings.get_next_sibling() is None

    last_item = config.filter("child 13", depth=None).one()
    assert last_item.get_next_sibling() is None

    has_next = config.filter("child 4", depth=None).one()
    the_next = config.filter("child 5", depth=None).one()
    assert has_next.get_next_sibling() is the_next


def test_get_prev(search_config):
    config = search_config
    child_7 = config.filter("child 7", depth=None).one()
    child_7_real_prev = config.filter("child 6", depth=None).one()
    assert child_7.get_prev() is child_7_real_prev

    child_9 = config.filter("child 9", depth=None).one()
    child_9_real_prev = config.filter("child 8", depth=None).one()
    assert child_9.get_prev() is child_9_real_prev

    first_item = config.filter("child 1", depth=None).one()
    assert first_item.get_prev() is None


def test_get_prev_sibling(search_config):
    config = search_config
    prev_line_is_prev_sibling = config.filter("child 7 next sibling", depth=None).one()
    prev_sibling = config.filter("child 7", depth=None).one()
    assert prev_line_is_prev_sibling.get_prev_sibling() is prev_sibling

    first_sibling = config.filter("child 4", depth=None).one()
    assert first_sibling.get_prev_sibling() is None

    no_siblings = config.filter("child 12", depth=None).one()
    assert no_siblings.get_prev_sibling() is None

    first_item = config.filter("child 1", depth=None).one()
    assert first_item.get_prev_sibling() is None

    has_prev = config.filter("child 5", depth=None).one()
    the_prev = config.filter("child 4", depth=None).one()
    assert has_prev.get_prev_sibling() is the_prev


def test_depth(search_config):
    config = search_config
    assert config.filter("child 1").one().depth == 0
    assert config.filter("child 4", depth=None).one().depth == 1
    assert config.filter("child 7", depth=None).one().depth == 1
    assert config.filter("child 9", depth=None).one().depth == 2
    assert config.filter("child 13", depth=None).one().depth == 1


def test_filter_children_w_child(search_config):
    config = search_config
    assert len(config.filter("child 3").filter_with_child("child 4")) == 1
    assert len(config.filter("child 3").filter_with_child("child 7")) == 0
    assert len(config.filter("child 1").filter_with_child("child 1")) == 0
    assert len(config.filter("child 1").filter_with_child("child 2")) == 0
    assert len(config.filter("dupe").filter_with_child("child 12")) == 1
    assert len(config.filter("dupe").filter_with_child("child 13")) == 1
    assert len(config.filter("dupe").filter_with_child("child.*")) == 2
    assert len(config.filter("sibling").filter_with_child("child 13")) == 0

    assert len(config.filter("child 6").filter_with_child("child 8")) == 0
    assert len(config.filter("child 6").filter_with_child("child 8", depth=None)) == 1

    print(config.filter("child 7", depth=None))
    assert len(config.filter("child 7").filter_with_child("child 8")) == 0
    assert (
        len(
            config.filter("child 7", depth=None).filter_with_child(
                "child 8", depth=None
            )
        )
        == 1
    )


def test_filter_children_wo_child(search_config):
    config = search_config
    assert len(config.filter("child 3").filter_without_child("child 4")) == 0
    assert len(config.filter("child.*").filter_without_child("child 4")) == 4
    assert len(config.filter("dupe").filter_without_child("child.*")) == 0

    assert len(config.filter("child 6").filter_without_child("child 8")) == 1
    assert (
        len(config.filter("child 6").filter_without_child("child 8", depth=None)) == 0
    )

    assert len(config.filter("child 7").filter_without_child("child 8")) == 0
    assert (
        len(config.filter("child 7").filter_without_child("child 8", depth=None)) == 0
    )

    assert len(config.filter("child 1").filter_without_child("child 4")) == 1
    assert (
        len(
            config.filter("child 1").filter_without_child(
                "child 4", skip_childless=True
            )
        )
        == 0
    )


def test_list_one(search_config):
    config = search_config
    assert len(config) > 1
    with pytest.raises(core.MultipleLinesError, message="Multi-item list should raise"):
        config.one()

    small_list = config.filter("silly child.*")
    assert len(small_list) == 1
    assert "silly" in small_list.one().text, "1-item list should return item"

    empty_list = core.ConfigLineList()
    assert len(empty_list) == 0
    with pytest.raises(core.NoLinesError, message="0-item list should raise"):
        empty_list.one()


def test_line_str_funcs():
    raw_text = "a fun test string with stuff string in here"
    line = core.ConfigLine(
        config_manager=None, parent=None, text=raw_text, line_number=2
    )
    assert line.find("fun") == 2
    assert line.find("not here") == -1
    assert line.rfind("fun") == 2
    assert line.rfind("not here") == -1

    assert line.index("fun") == 2
    with pytest.raises(ValueError):
        line.index("not here")
    assert line.rindex("fun") == 2
    with pytest.raises(ValueError):
        line.rindex("not here")

    assert line.count("s") == 4

    assert line.startswith("a fun")
    assert not line.startswith("test")
    assert line.endswith("in here")
    assert not line.endswith("stuff")

    assert line.upper() == raw_text.upper()
    assert line.lower() == raw_text.lower()

    assert line.partition("string") == raw_text.partition("string")
    assert line.rpartition("string") == raw_text.rpartition("string")

    assert line.split() == raw_text.split()
    assert line.rsplit() == raw_text.rsplit()

    assert len(line) == len(raw_text)
    assert line[4] == raw_text[4]


def test_line_compare():
    raw_text = "a fun test string with stuff in here"
    line1 = core.ConfigLine(
        config_manager=None, parent=None, text=raw_text, line_number=2
    )
    assert raw_text == line1.text
    assert raw_text == str(line1)
    assert raw_text == line1

    line2 = core.ConfigLine(
        config_manager=None, parent=None, text=raw_text, line_number=2
    )
    assert line1 != line2, "Lines must be the same instance to be equal"


def test_line_hash():
    raw_text = "a fun test string with stuff in here"
    line1 = core.ConfigLine(
        config_manager=None, parent=None, text=raw_text, line_number=2
    )

    assert hash(line1)

    # Should be able to put it as a key in a dict
    a = {line1: "work please"}
    assert a[line1] == "work please"
