from pathlib import Path
import os
import re
from textwrap import dedent
import pytest
import pyparsing

from .fixtures import *
from .context import parse, core


def config_checker(parent):
    """
    Ensure all children point to their parents, children don't have leading spaces, etc
    """
    for c in parent.children:
        assert c.text == c.text.strip(), "Children should not have useless spaces"
        assert c.parent == parent, "Children must point to their parent"
        config_checker(c)


def test_version_sorting():
    v1 = parse.VersionNumber(major=1, minor=1, revision=3)
    v2 = parse.VersionNumber(major=2, minor=1, revision=10)
    v3 = parse.VersionNumber(major=2, minor=2, revision=6)
    v4 = parse.VersionNumber(major=2, minor=10, revision=12)

    assert v1 < v2
    assert v2 < v3
    assert v3 < v4
    assert v1 < v4
    assert v1 == parse.VersionNumber(major=1, minor=1, revision=3)


def test_automatic(real_configs):
    os = real_configs["os"]
    contents = real_configs["contents"]
    if os == "ios":
        os = parse.ConfigIOS
    elif os == "asa":
        os = parse.ConfigASA
    elif os == "hp":
        os = parse.ConfigHPCommware
    elif os == "nxos":
        os = parse.ConfigNXOS
    elif os == "junos":
        os = parse.ConfigJunos
    elif os == "fortios":
        os = parse.ConfigFortigate
    else:
        raise ValueError("Unknown os type in test")

    assert isinstance(parse.automatic(contents), os)


def test_automatic_not_included(real_configs):
    os = real_configs["os"]
    contents = real_configs["contents"]

    with pytest.raises(parse.UnknownConfigError):
        parse.automatic(contents, include=[])


def test_automatic_not_included_fallback(real_configs):
    os = real_configs["os"]
    contents = real_configs["contents"]

    class ConfigFallback(parse.ConfigBase):
        """
        Do-nothing class that won't 'fail' parsing to use a fallback
        """

        def __init__(self, config_content):
            super().__init__(
                name="fallback",
                original_lines=config_content.splitlines(),
                comment_marker="!",
            )

    assert isinstance(
        parse.automatic(contents, include=[], fallback=ConfigFallback), ConfigFallback
    )


def test_ios_load(ios_config):
    config = parse.ConfigIOS(ios_config)
    assert config.tree_display()
    assert repr(config)
    assert len(config.original_lines) == len(
        ios_config.splitlines()
    ), "Didn't copy over lines to original correctly"
    assert (
        len(config.original_lines) == config[-1].line_number
    ), "Final line is not at the end of the file"
    assert config.parent is None

    banner_find = config.filter("banner login.*").one_or_none()
    if banner_find:
        banner_idx = config.get_line(banner_find.line_number)
        assert banner_idx.line_number == banner_find.line_number
        assert banner_idx.text == banner_find.text

    version = config.filter("version.*")
    if version:
        assert config.version.major in (12, 15, 16)
    else:
        assert config.version == None

    config_checker(config)


def test_nxos_load(nxos_config):
    config = parse.ConfigNXOS(nxos_config)
    assert config.tree_display()
    assert repr(config)
    assert len(config.original_lines) == len(
        nxos_config.splitlines()
    ), "Didn't copy over lines to original correctly"
    assert (
        len(config.original_lines) == config[-1].line_number
    ), "Final line is not at the end of the file"
    assert config.parent is None
    config_checker(config)


def test_junos_load(junos_config):
    config = parse.ConfigJunos(junos_config)
    assert config.tree_display()
    assert repr(config)
    assert config.parent is None
    config_checker(config)


def test_asa_load(asa_config):
    config = parse.ConfigASA(asa_config)
    assert config.tree_display()
    assert repr(config)
    assert len(config.original_lines) == len(
        asa_config.splitlines()
    ), "Didn't copy over lines to original correctly"
    assert (
        len(config.original_lines) == config[-1].line_number
    ), "Final line is not at the end of the file"
    assert config.parent is None

    version = config.filter(".*Version.*")
    if version:
        assert config.version.major == 9
    else:
        assert config.version == None

    config_checker(config)


def test_hp_load(hp_config):
    config = parse.ConfigHPCommware(hp_config)
    assert config.tree_display()
    assert repr(config)
    assert len(config.original_lines) == len(
        hp_config.splitlines()
    ), "Didn't copy over lines to original correctly"
    assert (
        len(config.original_lines) == config[-1].line_number
    ), "Final line is not at the end of the file"
    assert config.parent is None
    config_checker(config)

    version = config.filter("version.*")
    if version:
        assert config.version.major == 5
    else:
        assert config.version == None

    # ssh server enable is indented but is a top-level item
    # We should be able to find it without descending
    assert (
        config.filter("ssh server enable").one().parent == config
    ), "Indented top-level items are not properly placed"
    assert (
        config.filter("radius scheme system").one().parent == config
    ), "Un-indented top-level items are not properly placed"

    for scheme_line in config.filter("authentication-mode scheme", depth=1):
        # authentication-mode scheme is indented and has a parent
        assert (
            "user-interface" in scheme_line.parent.text
        ), "Indented top-level items are not properly placed"
        assert scheme_line.line_number in (337, 339)


def test_fortigate_load(fortigate_config):
    config = parse.ConfigFortigate(fortigate_config)
    assert config.tree_display()
    assert repr(config)
    assert len(config.original_lines) == len(
        fortigate_config.splitlines()
    ), "Didn't copy over lines to original correctly"
    assert (
        len(config.original_lines) == config[-1].line_number
    ), "Final line is not at the end of the file"
    assert config.parent is None
    config_checker(config)

    for line in config.filter("set private-key .*", depth=None):
        if "\n" not in line:
            assert "set private-key lines should be multiline to contain the whole key"

    # Line number should be correct in file
    for line in config.filter(
        'set public-key "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABA.*', depth=None
    ):
        assert line.line_number in (
            4052,
            4179,
            4213,
        ), "Line numbers for public keys did not match up, line number counts not maintained correctly"

    assert config.version.major in (5, 6)


def test_line_parse_checker():
    assert parse.line_parse_checker("there are no quotes here")
    assert parse.line_parse_checker("there are 'single quotes' here")
    assert parse.line_parse_checker('there are "double quotes" here')
    assert not parse.line_parse_checker('there are "unfinished quotes here')
    assert not parse.line_parse_checker("there are 'unfinished quotes here")
    assert parse.line_parse_checker('there are "escaped \\" quotes here" here')
    assert parse.line_parse_checker("there are 'escaped \\' quotes here' here")
    assert parse.line_parse_checker(
        'there are "double quotes\n that go across lines" here'
    )
    assert parse.line_parse_checker(
        'there are "escaped quotes \\" \n that go across lines" here'
    )
    assert not parse.line_parse_checker(
        'there are "unfinished quotes \\" \n that go across lines here'
    )


def test_get_line(ios_config):
    config = parse.ConfigIOS(ios_config)

    trials = ("exec-timeout 15 0", "banner login.*")
    for t in trials:
        trial_by_find = config.filter(t, depth=None).one_or_none()
        if trial_by_find:
            # import pdb; pdb.set_trace()
            trial_by_line = config.get_line(trial_by_find.line_number)
            assert trial_by_line.line_number == trial_by_find.line_number
            assert trial_by_line.text == trial_by_find.text


def test_one_indexed_list():
    original = [1, 2, 3, 4, 5]
    oneindexed = parse.OneIndexedList(original)

    # General getting
    for o in original:
        assert oneindexed[o] == o
    with pytest.raises(IndexError):
        oneindexed[0]
    with pytest.raises(IndexError):
        oneindexed[len(original) + 1], "Last element should be len(oneindexed)"

    # Reverse getting
    assert oneindexed[-1] == original[-1]
    assert oneindexed[-3] == original[-3]
    with pytest.raises(IndexError):
        oneindexed[-200]

    # Length
    assert len(oneindexed) == len(original)
    assert (
        oneindexed[len(oneindexed)] == original[-1]
    ), "Last element not accessible by length"

    # Slicing
    assert oneindexed[2:4] == original[1:3]
    assert oneindexed[:4] == original[:3]
    assert oneindexed[:-2] == original[:-2]

    oneindexed[2:4] = ["sliceset"]
    assert len(oneindexed) == len(original) - 1
    assert oneindexed[2] == "sliceset"

    # Setting
    oneindexed[1] = "set"
    assert oneindexed[1] == "set"
    with pytest.raises(IndexError):
        oneindexed[0] = 0
