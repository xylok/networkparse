from pathlib import Path
import os
import re
from textwrap import dedent
import pytest

from .context import parse


def file_loader(path):
    full_path = Path(__file__).resolve().parent / "samples" / path
    with open(full_path, "r") as f:
        return f.read()


@pytest.fixture(
    scope="module",
    params=[
        "sample_01.ios.txt",
        "sample_02.ios.txt",
        "sample_03.ios.txt",
        "sample_04.ios.txt",
        "sample_05.ios.txt",
        "sample_06.ios.txt",
        "sample_07.ios.txt",
        "sample_08.ios.txt",
        "sample_09.ios.txt",
        "sample_10.ios.txt",
        "small.ios.txt",
    ],
)
def ios_config(request):
    return file_loader(request.param)


@pytest.fixture(scope="module", params=["sample_01.nxos.txt"])
def nxos_config(request):
    return file_loader(request.param)


@pytest.fixture(scope="module", params=["sample_01.junos.txt", "sample_02.junos.txt"])
def junos_config(request):
    return file_loader(request.param)


@pytest.fixture(scope="module", params=["sample_01.asa.txt", "sample_02.asa.txt"])
def asa_config(request):
    return file_loader(request.param)


@pytest.fixture(scope="module", params=["sample_01.hpprocurve.txt"])
def hp_config(request):
    return file_loader(request.param)


@pytest.fixture(
    scope="module", params=["sample_01.fortigate.txt", "sample_02.fortigate.txt"]
)
def fortigate_config(request):
    return file_loader(request.param)


@pytest.fixture(
    scope="module",
    params=[
        "sample_01.ios.txt",
        "sample_02.ios.txt",
        "sample_03.ios.txt",
        "sample_04.ios.txt",
        "sample_05.ios.txt",
        "sample_06.ios.txt",
        "sample_07.ios.txt",
        "sample_08.ios.txt",
        "sample_09.ios.txt",
        "sample_10.ios.txt",
        "sample_01.fortigate.txt",
        "sample_02.fortigate.txt",
        "sample_01.nxos.txt",
        "sample_01.junos.txt",
        "sample_01.asa.txt",
        "sample_02.asa.txt",
        "sample_03.asa.txt",
        "sample_01.hpprocurve.txt",
    ],
)
def real_configs(request):
    name = request.param
    if "ios" in name:
        os = "ios"
    elif "forti" in name:
        os = "fortios"
    elif "nxos" in name:
        os = "nxos"
    elif "junos" in name:
        os = "junos"
    elif "asa" in name:
        os = "asa"
    elif "hp" in name:
        os = "hp"
    else:
        raise ValueError("Unable to determine sample OS")
    return {"os": os, "contents": file_loader(name)}
